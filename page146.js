// Listing 5.13 Using a closure in a timer interval callback
function animateIt(elementId) {

    var elem = document.getElementById(elementId);

    var tick = 0;

    var timer = setInterval(function () {

        if (tick < 100) {

            elem.style.left = elem.style.top = tick + "px";
            tick++;
            console.log(tick);
        } else {

            clearInterval(timer);

            console.log(tick === 100,
                "Tick accessed via a closure.");
            console.log(elem,
                "Element also accessed via a closure.");
            console.log(timer,
                "Timer reference also obtained via a closure.");

        }
    }, 10);
}

animateIt("box1");
animateIt("box2");