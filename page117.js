var outerValue = "samurai";
var later;

function outerFunction() {

    var innerValue = "ninja";

    function innerFunction() {
        console.log(outerValue === "samurai");
        console.log(innerValue === "ninja");
    }

    later = innerFunction;

}

// Appel de outerFunction qui permet de créer une reférence 
// de innerFunction() et de l'assigner à later
outerFunction();

later();