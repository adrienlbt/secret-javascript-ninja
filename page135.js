const GLOBAL_NINJA = "Yoshi";

function reportActivity() {

    const functionActivity = "jumping";

    for (let i = 1; i < 3; i++) {

        let forMessage = GLOBAL_NINJA + " " + functionActivity;

        console.log(forMessage === "Yoshi jumping");
        console.log("Current loop counter:" + i);
    }

    console.log(typeof i === "undefined" && typeof forMessage === "undefined");

}

reportActivity();

console.log(typeof functionActivity === "undefined" && typeof i === "undefined" && typeof forMessage === "undefined");