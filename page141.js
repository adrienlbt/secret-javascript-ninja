// Listing 5.11 Approximate private variables with closures

function Ninja() {

    // Declares a variable inside the constructor.Because the scope of the 
    // variable is limited to inside the constructor, it’s a 
    // “private” variable.
    var feints = 0;

    // An accessor method for the feints counter
    this.getFeints = function () {
        return feints;
    };

    // The increment method for the value.Because the value is private, 
    // no one can screw it up behind our backs; they’re limited to the 
    // access that we give them via methods.
    this.feint = function () {
        feints++;
    };
    
}

var ninja1 = new Ninja();

console.log(ninja1.feints === undefined,
    "And the private data is inaccessible to us.");

ninja1.feint();

console.log(ninja1.getFeints() === 1,
    "We're able to access the internal feint count.");

var ninja2 = new Ninja();

console.log(ninja2.getFeints() === 0,
    "The second ninja object gets its own feints variable.");