console.log(typeof fun === "function", "We access the function");
var fun = 3;
console.log(typeof fun === "number", "Now we access the number");
function fun() { }
console.log(typeof fun === "number", "Still a number");
