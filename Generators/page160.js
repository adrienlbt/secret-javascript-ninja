// Listing 6.8 Sending data to and receiving data from a generator

// A  generator can receive standard
// arguments, like any other function.
function* NinjaGenerator(action) {
    
    // The magic happens.By yielding a value, the
    // generator returns an intermediary calculation.
    // By calling the iterator’s next method with an 
    // argument, we send data back to the generator.
    const imposter = yield ("Hattori " + action);

    // The value sent over next becomes the
    // value of the yielded expression, so our
    // imposter is Hanzo.
    console.log(
        imposter === "Hanzo",
        "The generator has been infiltrated"
    );

    yield ("Yoshi (" + imposter + ") " + action);

}

// Normal argument passing
const ninjaIterator = NinjaGenerator("skulk");

// Triggers the execution of the generator and
// checks that we get the correct value
const result1 = ninjaIterator.next();
console.log(result1.value === "Hattori skulk", "Hattori is skulking");

// Sends data to the generator as an argument to the next method
// and checks whether the value was correctly transferred
const result2 = ninjaIterator.next("Hanzo");
console.log(
    result2.value === "Yoshi (Hanzo) skulk",
    "We have an imposter!"
);