// Listing 6.5 Using generators for generating IDs

// Defines an IdGenerator generator function
function* IdGenerator() {

    // A variable that keeps track of IDs. This variable can’t be modified
    // from outside our generator.
    let id = 0;

    // A variable that keeps track of IDs.This variable can’t be modified
    // from outside our generator.
    while (true) {
        yield ++id;
    }

}

// An iterator through which we’ll request new IDs from the generator
const idIterator = IdGenerator();

// Requests three new IDs
const ninja1 = { id: idIterator.next().value };
const ninja4 = { id:IdGenerator().next().value };
const ninja2 = { id: idIterator.next().value };
const ninja3 = { id: idIterator.next().value };

// Requests three new IDs
console.log(ninja1.id === 1, "First ninja has id 1");
console.log(ninja4.id === 1, "Fourth ninja has id 1");
console.log(ninja2.id === 2, "Second ninja has id 2");
console.log(ninja3.id === 3, "Third ninja has id 3");
