// Listing 6.6 Recursive DOM traversal
function traverseDOM(element, callback) {

    // Processes the current node with a callback
    callback(element);

    element = element.firstElementChild;

    // Traverses the DOM of each child element
    while (element) {
        traverseDOM(element, callback);
        element = element.nextElementSibling;
    }

}

// Starts the whole process by ccalling the traverseDOM 
// function for our root element
const subTree = document.getElementById("subTree");

traverseDOM(subTree, function (element) {
    console.log(element !== null, element.nodeName);
});

// ---------------------------------------------------------------------------------

// Listing 6.7 Iterating over a DOM tree with generators
function* DomTraversal(element) {

    yield element;

    element = element.firstElementChild;

    while (element) {

        // Uses yield* to transfer the iteration control 
        // to another instance of the DomTraversal generator
        yield* DomTraversal(element);
        element = element.nextElementSibling;

    }

}

// Iterates over the nodes by using the for-of loop
for (let element of DomTraversal(subTree)) {
    console.log(element !== null, element.nodeName);
}