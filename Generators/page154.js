// Listing 6.2 Controlling a generator through an iterator object

// Defines a generator that will produce a sequence of two weapons
function* WeaponGenerator() {
    yield "Katana";
    yield "Wakizashi";
}

// Calling a generator creates an iterator through which we control the 
// generator’s execution.
const weaponsIterator = WeaponGenerator();

// Calling a generator creates an iterator through 
// which we control the generator’s execution.
const result1 = weaponsIterator.next();

// The result is an object with a returned value and an indicator that 
// tells us whether the generator has more values.
console.log(
    typeof result1 === "object" && result1.value === "Katana" && !result1.done,
    "Katana received!"
);

// Calling next again gets another value from the generator
const result2 = weaponsIterator.next();
console.log(
    typeof result2 === "object" && result2.value === "Wakizashi" && !result2.done, 
    "Wakizashi received!"
);

// When there’s no more code to execute, the generator returns 
// “undefined” and indicates that it’s done.
const result3 = weaponsIterator.next();
console.log(
    typeof result3 === "object" && result3.value === undefined && result3.done, 
    "There are no more results!"
);