// Listing 6.3 Iterating over generator results with a while loop

function* WeaponGenerator() {

    yield "Katana";
    yield "Wakizashi";

}
const weaponsIterator = WeaponGenerator();

// Creates a variable in which we’ll store items of the generated sequence
let item;

while (!(item = weaponsIterator.next()).done) {

    console.log(item !== null, item.value);

}

// Comment moi j'aurai procédé
// let item = weaponsIterator.next();

// while(!item.done) {
//     console.log(item);
//     item = weaponsIterator.next();
// }