// Listing 6.4 Using yield * to delegate to another generator

function* WarriorGenerator() {

    yield "Sun Tzu";
    // yield * delegates to another generator
    yield* NinjaGenerator();
    yield "Genghis Khan";

}

function* NinjaGenerator() {

    yield "Hattori";
    yield "Yoshi";

}

for (let warrior of WarriorGenerator()) {
    console.log(warrior !== null, warrior);
}