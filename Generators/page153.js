// Listing 6.1 Using a generator function to generate a sequence of values

// Defines a generator function by putting * after the function keyword
function* WeaponGenerator() {

    // Generates individual values by using the new yield keyword
    yield "Katana";
    yield "Wakizashi";
    yield "Kusarigama";

}

// Consumes the generated sequence with the new for-of loop
for (let weapon of WeaponGenerator()) {
    console.log(weapon !== undefined, weapon);
}