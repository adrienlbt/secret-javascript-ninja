
function sum(...args) {

    var sum = 0;
    args.forEach((value) => {
        sum += value;
    })
    return sum;
}
console.log(sum(1,2,3));

function getSamurai(samurai) {
    "use strict"
    arguments[0] = "Ishida";
    return samurai;
}
function getNinja(ninja) {
    arguments[0] = "Fuma";
    return ninja;
}
var samurai = getSamurai("Toyotomi");
var ninja = getNinja("Yoshi");
console.log(samurai);
console.log(ninja);

function whoAmI1() {
    "use strict";
    return this;
}
function whoAmI2() {
    return this;
}

console.log(whoAmI1());
console.log(whoAmI2());

var ninja1 = {
    whoAmI: function () {
        return this;
    }
};
var ninja2 = {
    whoAmI: ninja1.whoAmI
};

var identify = ninja2.whoAmI;

console.log(ninja1.whoAmI());
console.log(ninja2.whoAmI());
console.log(identify());

function Ninja() {
    this.whoAmI = () => this;
}
var ninja1 = new Ninja();
var ninja2 = {
    whoAmI: ninja1.whoAmI
};

console.log(ninja1.whoAmI());
console.log(ninja2.whoAmI());

function Ninja() {
    this.whoAmI = function () {
        return this;
    }.bind(this);
}
var ninja1 = new Ninja();
var ninja2 = {
    whoAmI: ninja1.whoAmI
};

console.log(ninja1.whoAmI() === ninja1);
console.log(ninja2.whoAmI() === ninja2);