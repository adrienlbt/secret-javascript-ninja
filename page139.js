console.log(typeof fun === "function");
console.log(typeof myFunExp === "undefined");
console.log(typeof myArrow === "undefined");

function fun() { }

var myFunExpr = function () { };

var myArrow = (x) => x;