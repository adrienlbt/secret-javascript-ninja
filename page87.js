function whatever(a, b, c) {
    console.log(arguments);
}

whatever(1,2,3,4,5,6)


function sum() {
    var sum = 0;
    for (let i = 0; i < arguments.length; i++) {
        sum += arguments[i];
    }
    return sum;
}

console.log(sum(1,2,3,4));


function sumWithRestParameter(...number) {
    var sum = 0;
    number.forEach(element => {
        sum += element;
    })
    return sum;
}

console.log(sumWithRestParameter(1,2,3,4));
