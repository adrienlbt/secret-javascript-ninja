var globalNinja = "Yoshi";

function reportActivity() {
    
    var functionActivity = "jumping";

    for (var i = 1; i < 3; i++) {
        var forMessage = globalNinja + " " + functionActivity;
        console.log(forMessage === "Yoshi jumping");
        console.log("Current loop counter:" + i);
    }

    console.log(i === 3 && forMessage === "Yoshi jumping");

}

reportActivity();

console.log(typeof functionActivity === "undefined" && typeof i === "undefined" && typeof forMessage === "undefined"); 